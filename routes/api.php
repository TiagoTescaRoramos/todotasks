<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/tasks','TasksController@listTasks');
Route::post('/tasks/create','TasksController@createTasks');
Route::delete('/tasks/delete/{id}','TasksController@deleteTasks');
Route::put('/tasks/edit','TasksController@editTasks');
Route::put('/tasks/finish/{id}','TasksController@finishTasks');