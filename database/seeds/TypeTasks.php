<?php

use Illuminate\Database\Seeder;

class TypeTasks extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('typeTasks')->insert(['name' => 'Shopping']);
        DB::table('typeTasks')->insert(['name' => 'Work']);
    }
}
