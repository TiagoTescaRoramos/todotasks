<?php

use Illuminate\Database\Seeder;

class StatusTasks extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statusTasks')->insert(['name' => 'Pending']);
        DB::table('statusTasks')->insert(['name' => 'Finish']);
    }
}
