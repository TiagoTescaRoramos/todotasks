<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('tasks')) {
            return false;
        }

        Schema::create('tasks', function(Blueprint $table){
            $table->increments('id');
            $table->integer('idUsuario');
            $table->string('name');
            $table->string('description');
            $table->uuid('uuid')->unique();
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->integer('idTypeTasks');
            $table->integer('idStatus');
            $table->integer('priority');
            $table->foreign('idTypeTasks')->references('id')->on('typeTasks') ;
            $table->foreign('idStatus')->references('id')->on('statusTasks');
            $table->tinyInteger('active')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
