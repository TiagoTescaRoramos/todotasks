<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TasksTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTasksListAll()
    {
        $response = $this->get('/api/tasks');
        $response->assertStatus(200);
    }

    public function testTasksListWork()
    {
        $response = $this->get('/api/tasks',[
            'type' => 'work'
        ]);
        $response->assertStatus(200);
    }

    public function testTasksListShopping()
    {
        $response = $this->get('/api/tasks',[
            'type' => 'shopping'
        ]);
        $response->assertStatus(200);
    }

    public function testTasksCreateValidate()
    {
        $response = $this->post('/api/tasks/create',['name'=>'']);
        $response->assertStatus(200);
        $response->assertSeeText('Bad Move');
    }

    public function testTasksCreateValidate2()
    {
        $response = $this->post('/api/tasks/create');
        $response->assertStatus(200);
        $response->assertSeeText('Bad Move');
    }

    public function testTasksCreateSuccess()
    {
        $response = $this->post('/api/tasks/create',[
            'name' => 'New Car',
            'description' => 'I need to buy a new red car.',
            'type' => 'work',
            'order' => '2'
        ]);
        $response->assertSeeText('Tasks Create');
    }

    public function testTasksCreateSuccessPriority()
    {
        $response = $this->post('/api/tasks/create',[
            'name' => 'Car',
            'description' => 'I need to sell my red car.',
            'type' => 'work',
            'order' => '1'
        ]);
        $response->assertSeeText('Tasks Create');
    }

    public function testTasksCreateError()
    {
        $response = $this->post('/api/tasks/create',[
            'name' => 'New Car',
            'description' => 'I need to buy a new red car.',
            'type' => 'Test',
            'order' => '2'
        ]);
        $response->assertSeeText('The task type you provided is not supported. You can only use shopping(1) or work(2).');
    }

    public function testTasksDeleteSuccess()
    {
        $response = $this->delete('/api/tasks/delete/18'); // You can to change the id, case you need to make a new test
        $response->assertStatus(200);
    }

    public function testTasksDeleteError()
    {
        $response = $this->delete('/api/tasks/delete/18'); //This id is active=0, not found. You can to change the id, case you need to make a new test
        $response->assertSeeText("Good news! The task you were trying to delete didn't even exist.");
    }

    public function testTasksEditSuccess()
    {
        $response = $this->put('/api/tasks/edit',[
            'name' => 'New Test',
            'description' => 'Test Success.',
            'order' => '2',
            'id' => 20 // You can to change the id, case you need to make a new test
        ]);
        $response->assertDontSeeText("Are you a hacker or something? The task you were trying to edit doesn't exist.");
    }

    public function testTasksEditError()
    {
        $response = $this->put('/api/tasks/edit',[
            'name' => 'New Test',
            'description' => 'Test Success.',
            'order' => '2',
            'id' => 999 // This id don't exist, not found. You can to change the id, case you need to make a new test
        ]);
        $response->assertSeeText("Are you a hacker or something? The task you were trying to edit doesn't exist.");
    }

    public function testTasksFinishSuccess()
    {
        $response = $this->put('/api/tasks/finish/23'); // You can to change the id, case you need to make a new test
        $response->assertStatus(200);
    }

}
