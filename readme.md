## TodoTasks

É um sistema feito em Laravel 5.6, para tarefas. Nele podemos inserir tarefas, deletar tarefas, alterar tarefas e 
listar tarefas inseridas.

## Instalando

Clona a pasta, entra na pasta e executa o comando:

**docker-compose up**

Lembrando que você precisa ter instalado o docker e o docker-compose na sua máquina.
Depois de ter executado o comando, entra no container com o comando:

**docker exec -it todoTaskWEB bash**

O sistema já tem criado as migrates. Mas antes de executar as migrates, você precisa criar a database, com comando:

**CREATE DATABASE todoTask;**

Para executar para executar as migrates, execute os comandos:

**php artisan migrate**

**php artisan db:seed --class=StatusTasks**

**php artisan db:seed --class=TypeTasks**

## Dependências

O servidor PHP precisa ser o PHP7.1, como está no *docker-composer.yml*. No servidor, é preciso ter 
instalado o pdo e o pdo-mysql.

## Contato

**Skype:** tiago_tescaro

**Email:** tiago.tescaro@hotmail.com

Você consegue me localizar no slacks do *phpsp*.