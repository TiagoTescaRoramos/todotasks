<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tasks;
use App\TypeTasks;
use Illuminate\Support\Str;

class TasksController extends Controller
{
    public function listTasks(Request $request)
    {
        $tasks = Tasks::where('active',1)
            ->where('idStatus' , '1')
            ->orderBy('priority','asc')
            ->orderBy('created_at','asc');

        if (!empty($request->type)) {
            $typeTasks = TypeTasks::where('name', (string)$request->type)->get();
            if (count($typeTasks) >= 0) {
                $tasks->where('idTypeTasks' , $typeTasks[0]['id']);
            }
        }
        $dados = $tasks->get();
        if (count($dados) <= 0) {
            return  'Wow. You have nothing else to do. Enjoy the rest of your day!';
        }
        return response()->json($dados);
    }

    public function editTasks(Request $request)
    {
        if (empty($request->name) || empty($request->description)) {
            return 'Bad Move';
        }

        $tasks = Tasks::where('id',$request->id)->where('active',1);
        $tasksGet = $tasks->get();
        if (count($tasksGet) <= 0) {
            return "Are you a hacker or something? The task you were trying to edit doesn't exist.";
        }
        $tasks->update([
            'name' => $request->name,
            'description' => $request->description,
            'priority' => $request->order
        ]);
    }

    public function finishTasks($id)
    {
        $tasks = Tasks::where('id',$id)->where('active',1)->where('idStatus', 1);
        $tasksGet = $tasks->get();
        if (count($tasksGet) <= 0) {
            return "Good news! The task you were trying to delete didn't even exist.";
        }
        $tasks->update(['idStatus' => '2']);
    }

    public function createTasks(Request $request)
    {
        if (empty($request->name) || empty($request->description)) {
            return 'Bad Move';
        }

        $typeTasks = TypeTasks::where('name',(string)$request->type)->get();
        if (count($typeTasks) <= 0) {
            return 'The task type you provided is not supported. You can only use shopping(1) or work(2).';
        }

        $tasks = new Tasks();
        $tasks->name = $request->name;
        $tasks->description = $request->description;
        $tasks->uuid = Str::uuid();
        $tasks->idTypeTasks = $typeTasks[0]['id'];
        $tasks->idStatus = 1;
        $tasks->priority = $request->order;
        $tasks->idUsuario = 1;

        $tasks->save();
        return 'Tasks Created';
    }

    public function deleteTasks($id)
    {
        $tasks = Tasks::where('id',$id)->where('active',1);
        $tasksGet = $tasks->get();
        if (count($tasksGet) <= 0) {
            return "Good news! The task you were trying to delete didn't even exist.";
        }
        $tasks->update(['active' => '0']);
    }
}
